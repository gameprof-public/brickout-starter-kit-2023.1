using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Brick : MonoBehaviour {

    void OnCollisionEnter( Collision collision ) {
        // Did the Puck hit us?
        Puck puck = collision.gameObject.GetComponent<Puck>();
        if (puck != null) {
            Destroy( gameObject );
        }
    }

}
