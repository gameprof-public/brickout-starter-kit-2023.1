using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrickLayer : MonoBehaviour {
    [Header( "Inscribed" )]
    public GameObject   brickPrefab;
    public Vector2      brickSpacing = new Vector2(2, 0.75f);
    public Vector2Int   brickColumnsRows = new Vector2Int( 10, 4 );
    public float        startY = 1;

    void Start() {
        PlaceBricks();
    }

    void PlaceBricks() {
        float startX = brickSpacing.x * brickColumnsRows.x;
        startX /= -2f;
        startX += brickSpacing.x * 0.5f;
        float xOffset = brickSpacing.x * 0.25f;
        for (int row=0; row<brickColumnsRows.y; row++) {
            xOffset *= -1;
            for (int col=0; col<brickColumnsRows.x; col++ ) {
                GameObject go = Instantiate<GameObject>( brickPrefab );
                go.transform.position = new Vector3( startX + ( col * brickSpacing.x ) + xOffset,
                                                     startY + ( row * brickSpacing.y ) );
            }
        }
    }
}
