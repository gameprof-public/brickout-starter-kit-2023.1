using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Puck : MonoBehaviour {
    public float startSpeed = 10;
    public float speedIncreaseOnBounce = 0.5f;

    Rigidbody rigid;

    void Start() {
        rigid = GetComponent<Rigidbody>();
        Reset();
    }

    void Reset() {
        transform.position = Vector3.zero;
        // Move downward to begin with
        Vector3 vel = Vector3.down;
        vel.x = Random.Range( -1f, 1f );
        vel.Normalize();
        vel *= startSpeed;

        rigid.velocity = vel;
    }

    void OnCollisionEnter( Collision collision ) {
        Vector3 vel = rigid.velocity;
        float speed = vel.magnitude;
        speed += speedIncreaseOnBounce;
        vel = vel.normalized * speed;
        // If this hit the Paddle
        Paddle pad = collision.gameObject.GetComponent<Paddle>();
        if ( pad != null ) {
            // Bounce off at an angle
            float xDelta = transform.position.x - pad.transform.position.x;
            xDelta = xDelta * 2 / pad.transform.localScale.x;
            vel = new Vector3( xDelta, 1, 0 );
            vel = vel.normalized * speed;
        }

        rigid.velocity = vel;
    }

    void FixedUpdate() {
        if ( transform.position.y < -10 ) Reset();
    }
}
