using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Paddle : MonoBehaviour {
    [Header( "Inscribed" )]
    public float xLimit = 13;

    Vector3 startPos;

    void Start() {
        startPos = transform.position;
    }

    void Update() {
        // Get the mouse position on screen
        Vector3 mousePos2D = Input.mousePosition;
        mousePos2D.z = 10;

        // Convert this to a 3D world position
        Vector3 mousePos3D = Camera.main.ScreenToWorldPoint( mousePos2D );

        // Limit the X position of the puck
        Vector3 pos = startPos;
        pos.x = Mathf.Clamp( mousePos3D.x, -xLimit, xLimit );

        // Move the puck to the right X position
        transform.position = pos;
    }
}
